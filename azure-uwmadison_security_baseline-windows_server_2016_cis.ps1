# Global configuration
$sourceZip = "https://git.doit.wisc.edu/steve-tanner/azure-uwmadison_security_baseline-windows_server_2016_cis.public/raw/master/azure-uwmadison_security_baseline-windows_server_2016_cis.zip"
$sourceChecksumFile = "https://git.doit.wisc.edu/steve-tanner/azure-uwmadison_security_baseline-windows_server_2016_cis.public/raw/master/azure-uwmadison_security_baseline-windows_server_2016_cis.zip.sha256"
$destinationZip = "C:\azure-uwmadison_security_baseline-windows_server_2016_cis.zip"
$extractPath = "C:\azure-uwmadison_security_baseline-windows_server_2016_cis\"
$localCommandPath = $extractPath
$localCommandInput = $extractPath + "input.txt"
$localCommand = "uwmadison-security_baseline.cmd"

# Download ZIP, ZIP checksum
try {
    $webClient = New-Object System.Net.WebClient
    $webClient.DownloadFile($sourceZip, $destinationZip)
    $sourceChecksum = $webClient.DownloadString($sourceChecksumFile)
}
catch {
    
    Write-Host "Unable to download ""$sourceZip"" as ""$destinationZip"""
    # Write-Host $_.Exception.Message
    Exit 4
}

# Verify ZIP checksum
$destinationChecksum = (Get-FileHash -LiteralPath $destinationZip -Algorithm SHA256).Hash
if ($sourceChecksum -ne $destinationChecksum) {
    Write-Host "SHA-256 checksum verification of ""$destinationZip"" failed"
    # Write-Host "Deleting ""$destinationZip"""
    # Remove-Item -LiteralPath $destinationZip
    Exit 5
}

# Extract ZIP
try {
    # $shell = New-Object -ComObject Shell.Application
    # $shell.NameSpace($extractPath).CopyHere(($shell.NameSpace($destinationZip)).items())
    Add-Type -AssemblyName System.IO.Compression.FileSystem
    [System.IO.Compression.ZipFile]::ExtractToDirectory($destinationZip, $extractPath)
}
catch {
    Write-Host "Unable to extract ""$destinationZip"" into ""$extractPath"""
    # Write-Host $_.Exception.Message
    Exit 4
}

# Run post install configuration script
try {
    Start-Process -FilePath "$localCommandPath$localCommand" -WorkingDirectory "$localCommandPath" -RedirectStandardInput "$localCommandInput" -Wait
}
catch {
    Write-Host "Unable to execute ""$localCommand"" from ""$localCommandPath"""
    # Write-Host $_.Exception.Message
}
